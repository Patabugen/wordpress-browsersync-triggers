# WordPress Browsersync Triggers #

Browsersync Triggers for WordPress.

Integrates [WordPress](https://wordpress.org) with [Browsersync](https://www.browsersync.io/) to trigger events like Reload when you edit
pages and settings.

See [browsersync-triggers/readme.txt](https://bitbucket.org/Patabugen/wordpress-browsersync-triggers/src/717016f2c984bf966d3867659e5a1627eb59578b/browsersync-triggers/?at=master) for full details.

## Quickstart ##
This is the repository for the code, if  you just want to get the plugin download
[browsersync-triggers.zip](https://bitbucket.org/Patabugen/wordpress-browsersync-triggers/src/master/browsersync-triggers.zip?at=master&fileviewer=file-view-default) and upload it to your WordPress site.

For full (and essential) installation instructions, see browsersync-triggers/readme.txt

## Contribution guidelines ##

* All feedback, requests or contributions are welcome
* Stick to WordPress Coding Standards (however much you hate them)
* Send me a pull request

## Contact ##

* Contact Sami here: [http://patabugen.co.uk/contact/](http://patabugen.co.uk/contact/)